# VenenuX Simple alpine lighttpd docker

IMPORTANT: Before continuing we must have installed Debian 9, Debian 8, VenenuX 11, VenenuX 9
- docker: https://docs.docker.com/install/
- docker-compose: https://docs.docker.com/compose/install/

## Usage

0. Devel:

`mkdir ~/Devel`

1. Clone: 

`cd ~/Devel && git clone https://gitlab.com/venenux/vnx-dockers-containers`.

2. Build:

`cd ~/Devel/simple-containers/lighttpd && docker-compose up --build`

5. Use:

`docker exec -it lighttpd bash`

7. Run

`docker-compose up`

